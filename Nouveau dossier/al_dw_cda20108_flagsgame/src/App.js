import MyFlags from './components/MyFlags';
import './App.css';
import Accueil from './components/Accueil';
import React, {useState} from 'react';



const App = () =>{

  const [start, setStart] = useState(false)
  const [abandon, setAbandon] = useState(false)
  
  return (
    <div className="App">
      <Accueil setStart ={setStart} start={start}/>
      <MyFlags start={start} setStart ={setStart}/> 
    </div>
  );
}

export default App;
