import React from 'react'

const FlagFoundAbandon = props => {

    if(!props.show){
    return null;
    }
    return (
        <div className="modal" onClick={props.onClose}>
        <div className="modal-content" onClick={e => e.stopPropagation()}>
            <h4 className="modal-title">Vous n'avez pas trouvé les couleur de {props.pays} !</h4>
            <div className="modal-body">Vous avez gagné 0 pts</div>
            <div className="modal-footer">
                <button className="button-modal" onClick={props.onClose}>Fermer</button>
            </div>
        </div> 
    </div>
    )
}

export default FlagFoundAbandon

