import React from 'react'

const FlagFoundModal = props => {

    if(!props.show){
        return null;
    }

    return (
        <div className="modal" onClick={props.onClose}>
            <div className="modal-content" onClick={e => e.stopPropagation()}>
                <h4 className="modal-title">Vous avez trouvé les couleurs de {props.pays} !</h4>
                <div className="modal-body">Vous avez gagné {props.score}pts</div>
                <div className="modal-footer">
                    <button className="button-modal" onClick={props.onClose}>Fermer</button>
                </div>
            </div> 
        </div>
    )
}

export default FlagFoundModal
