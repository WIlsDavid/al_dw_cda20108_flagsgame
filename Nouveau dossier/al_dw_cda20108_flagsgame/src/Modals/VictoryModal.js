import React from 'react'
import "./Modal.css"

const VictoryModal = props => {
    if(!props.show){
        return null;
    }

    return (
        <div className="modal" onClick={props.onClose}>
            <div className="modal-content" onClick={e => e.stopPropagation()}>
                <h4 className="modal-title">Victoire !</h4>
                <div className="modal-body">Vous avez gagné !!! les drapeaux n'ont plus de secrets pour vous</div>
                <div className="modal-footer">
                    {/* <input type="button" onClick={props.onClose} value="Fermer"/> */}
                </div>
            </div> 
        </div>
    )
}

export default VictoryModal
