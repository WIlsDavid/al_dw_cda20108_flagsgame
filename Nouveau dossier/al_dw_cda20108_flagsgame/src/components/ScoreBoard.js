import React, {useState, useEffect} from 'react'
import './ScoreBoard.css'

const ScoreBoard = props => {

    const ajusterLevel = (props.flagNo + 1 > props.maxFlags) ? props.maxFlags : props.flagNo;

    return (
        <div className="ScoreBoard">
            <div>
                <div>
                    Score 
                </div>
                <div>
                    {0}
                </div> 
            </div>
            <div>
                <div>
                    Nombres de clics
                </div>
                <div>
                    {props.clicks}
                </div>
            </div>
            <div>
                <div>
                    Level
                </div>
                <div> {ajusterLevel +1} / {props.maxFlags}</div>
            </div>
            
            
        </div>
    )
}

export default ScoreBoard
