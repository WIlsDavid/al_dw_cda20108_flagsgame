import React, { useState, useRef, useEffect } from 'react'
import Bande from './Bande';
import FlagFoundModal from '../Modals/FlagFoundModal';
import './Flag.css';


const Flag = ({pays , victoryTest , updateVictoryTest , setClicks, clicks}) => {

    const [colorBandes, setColorBandes] = useState([]);
    const countryRef = useRef(null);
    const [infoBande1, updateInfoBande1] = useState(pays.colorSet[2]);
    const [infoBande2, updateInfoBande2] = useState(pays.colorSet[2]);
    const [infoBande3, updateInfoBande3] = useState(pays.colorSet[2]);
    const [show, setShow] = useState(false);
    let scoreDrapeau;

    const [time, setTime] = useState(0);
    const [timerOn, setTimerOn] = useState(false);

    const [dataPlayer, setDataPlayer] = useState([JSON.parse(localStorage.getItem("dataPlayer"))]);
    const [exitPlayer, setExitPlayer] = useState([JSON.parse(localStorage.getItem("exitPlayer"))]);

    window.addEventListener('beforeunload', (event) => {
        setExitPlayer([]);
        updateLocalStorageSortie();
        
    });
    
    

    // Permet d'initialiser les valeurs des infoBandes : compare le pays (current) avec le nouveau pays
    // utilisation des Hooks : useRef et useEffect, 
    //useRef permet de stocké la valeur "d'origine" d'une variable avec l'attribut current
    //useEffect  se charge du montage,de la modification ou de la suppression du state (cycle de vie)
    useEffect(() => {
        if(countryRef.current != pays.name){
            // setColorBandes([]);
            // countryRef.current = pays.name;
            // for (let i = 0; i < pays.nbBandes; i++) {
            //     setColorBandes(arr => [...arr, pays.colorSet[2]]);
            // }
            updateInfoBande1(pays.colorSet[2]);
            updateInfoBande2(pays.colorSet[2]);
            updateInfoBande3(pays.colorSet[2]);
            setTime(0);
            setClicks(0);
            setTimerOn(true);
            let interval = null;
        
            if (timerOn){
                interval = setInterval(() => {
                    setTime(temps => temps + 1);
                }, 1000);
            } else {
            clearInterval(interval)
        }  
        return () => clearInterval(interval)
        }
    }, [pays.name, timerOn])

    useEffect(() => {
        //console.log("color bande =>" + colorBandes);
        checkVictory();
    })
    
    
    const checkVictory = () => {
        console.log(infoBande1 + " , "+ infoBande2 + " , " + infoBande3 + " , " + clicks );
        let victory = false;
        if( pays.name == "Pologne" && infoBande1 == pays.colorOrder[0] && infoBande2 == pays.colorOrder[1]){
            victory = true;
        }else if(pays.name != "Pologne" && infoBande1 == pays.colorOrder[0] && infoBande2 == pays.colorOrder[1] && infoBande3 == pays.colorOrder[2]){
            victory = true;
        }
        if(victory){
            calculScore();
            updateLocalStorage();
            updateVictoryTest(prevVictoryTest => prevVictoryTest + 1);
        }
    }

    const calculScore = () =>{
        if( time <= pays.optiClic-2){
            if (clicks < pays.optiClic) {
                scoreDrapeau = 3;
            } else {
                scoreDrapeau = 2;
            }
        }else{
            scoreDrapeau = 1;
        }
    }

    const updateLocalStorage = () => {
        const infoPlayer = [...dataPlayer];
        const newData = {
            pays: pays.name,
            drapeauNo : victoryTest + 1,
            time : time,
            nbCoups : clicks
        }
        
        infoPlayer.push(newData);
        setDataPlayer(infoPlayer);
        localStorage.setItem("dataPlayer", JSON.stringify(dataPlayer));
        console.log(localStorage.getItem("dataPlayer"));
        setShow(true);
    }

    const updateLocalStorageSortie = () => {
        const infoPlayer = exitPlayer;
        const newData = {
            pays: pays.name,
            drapeauNo : victoryTest + 1,
            time : time,
            nbCoups : clicks,
            infoBande1: infoBande1,
            infoBande2: infoBande2,
            infoBande3: infoBande3
        }
        infoPlayer.push(newData);
        setExitPlayer(infoPlayer);
        localStorage.setItem("exitPlayer", JSON.stringify(exitPlayer));
    }

    const stopChrono = () =>{
        setTimerOn(false)
    }



    return (
        <div className="board">
            <div className="pays">
                <p>{pays.name}</p>
                <div>
                <span>{("0" + (Math.floor((time/60) % 60))).slice(-2)}</span> :
                <span>{("0" + ((time) % 60)).slice(-2)}</span>
            </div>
            </div>
            <div className={"flag "+ pays.disposition}>
                <Bande bid={1} updateInfoBande={updateInfoBande1} color={pays.colorSet} dispo={pays.disposition} setClicks={setClicks}/>
                <Bande bid={2} updateInfoBande={updateInfoBande2} color={pays.colorSet} dispo={pays.disposition} setClicks={setClicks}/>
                {(pays.name != "Pologne") && <Bande bid={3} updateInfoBande={updateInfoBande3} color={pays.colorSet} dispo={pays.disposition} setClicks={setClicks}/>}
                
            </div>
            <div className="btn-box">
                <FlagFoundModal show={show} onClose={() => setShow(false)} pays={countryRef.current} score={scoreDrapeau} />
                <input type="button" value="Stop Chrono"/>

            </div>
        </div>
    )
}
export default Flag;
