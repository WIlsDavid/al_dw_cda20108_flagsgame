import React, {useState} from 'react'
import Flag from './Flag'
import  TableauParti from  './TableauParti'
import BoutonParti from './BoutonParti'
import VictoryModal from '../Modals/VictoryModal'
import ScoreBoard from './ScoreBoard';

export const MyFlags = props => {


    const [victoryTest, updateVictoryTest] = useState(0);
    const [load, setLoad] = useState(false);
    const [clicks, setClicks] = useState(0);
    const [scoreTotal, setScoreTotal] = useState(0);

    
    if(!props.start){

        return null
    }

    const countries = [
            { 
                name : "France" ,
                nbBandes : 3 ,
                disposition : "vertical",
                colorSet : ["white","red","blue"] ,
                colorOrder : ["blue","white","red"],
                optiClic : 3
            },
            {
                name : "Belgique" ,
                nbBandes : 3 ,
                disposition : "vertical",
                colorSet : ["red","black","yellow"] ,
                colorOrder : ["black","yellow","red"],
                optiClic : 3
            },
            {
                name : "Allemagne" ,
                nbBandes : 3 ,
                disposition : "horizontal",
                colorSet : ["yellow","black","red"] ,
                colorOrder : ["black","red","yellow"],
                optiClic : 5
            },
            {
                name : "Pays Bas" ,
                nbBandes : 3 ,
                disposition : "horizontal",
                colorSet : ["white","red","blue"] ,
                colorOrder : ["red","white","blue"],
                optiClic : 5
            },
            {
                name : "Republique Tcheque" ,
                nbBandes : 3 ,
                disposition : "triangle",
                colorSet : ["red","blue","white"] ,
                colorOrder : ["blue","white","red"],
                optiClic : 5
            },
            {
                name : "Pologne" ,
                nbBandes : 2 ,
                disposition : "horizontal2",
                colorSet : ["red","blue","white"] ,
                colorOrder : ["white","red"],
                optiClic : 3
            }
        ]
        console.log("index pays " + victoryTest);
        const nextCountry = (victoryTest < countries.length) ? <Flag pays={countries[victoryTest]} victoryTest={victoryTest} updateVictoryTest={updateVictoryTest} clicks={clicks} setClicks={setClicks} scoreTotal={scoreTotal} setScoreTotal={setScoreTotal}/> : <VictoryModal show={true}/>



    return (
        <div className="MyFlags">
            <ScoreBoard maxFlags={countries.length} flagNo={victoryTest} clicks={clicks} setClicks={setClicks}/>
            {nextCountry}
            <BoutonParti victoryTest={victoryTest} updateVictoryTest={updateVictoryTest}/>
            <TableauParti/>
            
            
        </div>
    )
}
export default MyFlags;
