import React from 'react'
import './Accueil.css'

const Accueil = props => {
    
    
    
    if(props.start){
        return null
    }

    return (
        <div className="modal" onClick={props.onClose}>
            <div className="modal-content" onClick={e => e.stopPropagation()}>
                <h4 className="modal-title"><h1>Le jeux des drapeaux</h1></h4>
                <div className="modal-body"> 
                    <p>Bienvenue aux jeux des drapeaux.</p><br/>
                    <p>Il s'agit d'un petit jeux où le but est de trouver tout les drapeaux.</p><br/>
                    <p>Le temps et le nombre de click de la souris seront comptabilisés pour comparer vos connaissances et votre rapidité par rapport à d'autres joueurs </p>
                </div>
                <div className="modal-footer">
                    <button onClick = {() => props.setStart(true) }>Commencer</button>
                    
                </div>
            </div> 
        </div> 
    )
}


export default Accueil 
