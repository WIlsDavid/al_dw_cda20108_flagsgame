import React, {useState, useEffect} from 'react'

const Bande = ({updateInfoBande,color,dispo,bid, setClicks}) => {

    const [indexColor, setIndexColor] = useState(0);
    const max = color.length;

    const changeColor = (e) => {
        setClicks(prevClicks => prevClicks + 1)
        for (let i = 0 ; i < max ; i++) {
            e.target.classList.remove(color[i]);
        }
        if(indexColor == max-1){
            setIndexColor(0);
        }else{
            setIndexColor(prevIndexColor => prevIndexColor + 1);
        }
        e.target.classList.add(color[indexColor]);
        updateInfoBande(color[indexColor]);
        
    }

    

    return (
        <div id= {"bande"+ dispo} className= {"bande " + color[2] + " "+ "bande"+ bid} onClick={changeColor}>
        </div>
    )
}

export default Bande
