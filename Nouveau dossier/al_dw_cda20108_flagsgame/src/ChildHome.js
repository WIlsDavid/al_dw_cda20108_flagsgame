import React from 'react'

const ChildHome = props => {
    let { item, arr, setItem } = props;

function onToggle(it) {
    let newArr = arr.map(data => {
    data.isOpen = data.title == it.title;
    return data;
    });
    setItem(newArr);
}
return (
    <div key={item.title}>
    <label onClick={() => onToggle(item)}>{item.title}</label>
    {item.isOpen && <p>{item.description}</p>}
    </div>
);
}

export default ChildHome
